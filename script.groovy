def buildApp() {
    echo "building the applicaton..."
}

def testApp() {
    echo "testing the application..."
}

def deployApp() {
    echo "deploying the application..."
    echo "deploying version ${params.VERSION}"
}

return this
