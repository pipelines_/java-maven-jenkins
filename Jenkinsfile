def gv //defining a variable.

pipeline { //defining the pipeline job.

    agent any //agent: states where to execute or run script; any: runs scripton any jenkins agent.

    parameters { //defines the parameters necessary to access external configurations.
        choice(name: "VERSION", choices: ["1.1.0", "1.2.0","1.3.0"], description: "")
        booleanParam(name: "executeTests", defaultValue: true, description: "")
    }

    stages{ //where the job happens.

        stage("init") { //defines the stage of job. "init" however, initiates a groovy script.

            steps { //defines every steps for a specific stage of job.

                script { //this permits access to groovy scripts
                    gv = load "script.groovy" //assigns the attribute of loading a groovy scipt to a variable.
                }
            }
        }

        stage("build") {

            steps {

                script {
                    gv.buildApp() //executes the "buildApp" function which exists in groovy script.
                }
            }
        }

        stage("test") {
            
            when { //defines a conditional expression for the execution of the steps in this stage.

                expression { //the expression below asserts that the parameter "executeTests" is true.
                    params.executeTests
                }
            }

            steps {

                script {
                    gv.testApp() //executes the "testApp" function which exists in groovy script.
                }
            }
        }

        stage("deploy") {

            input {
                message "select environment to deploy to"
                ok "Select"
                parameters {
                    choice(name: "DEV_A", choices: ["development", "staging", "production"], description: "")
                    choice(name: "DEV_B", choices: ["development", "staging", "production"], description: "")
                }
            }

            steps {

                script {
                    gv.deployApp() //executes the "deployApp" function which exists in groovy script.
                    echo "deploying to ${DEV_A} and ${DEV_B} environments..."
                    
                    env.ENV = input message: "select cloud platform", ok: "Select", parameters: [choice(name: "ENV", choices: ["AWS", "GCP", "M.azure"], description: "")]

                    echo "setting up ${ENV} platform..."
                }
            }
        }
    }   
}
